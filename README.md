# Magicwords

Flexible publishing pipelines via document-driven syntax.

## Description

Magicwords are *points of intervention* allowing the collective writing of documents that mixes information from other sources, involves negotiable pipelines of (pre/post) processing. The goal of Magicwords is to enable more decisions to be negotiated in the Document itself rather than hard coded into hidden pipeline code only accessible to *system programmers*, in this way resisting the way rigid document processing pipelines harden and isolate roles of programming, editorial work, and design.

This code is inspired by [the same-named feature found in Mediawiki](https://www.mediawiki.org/wiki/Help:Magic_words). It is being developed for use in the work of the Brussels-based non-profit [Constant](https://constantvzw.org).

Mediawiki magicwords can serve one of three purposes:
* Behaviour switches
* Variables
* Parser functions

Magicwords allow flexbile/customizable document transformation workflows where interventions can be made directly in the document itself, rather than in externally defined configuration / customization. Magicwords can serve multiple functions:

* Special variables / functions that are translated (in-place) into other text
* Pipeline / Behaviour switches that filter how the entire document gets (post) processed (and where the position in a text doesn't matter).


Specifically, this work has been designed to support:

* inclusion/transclusion, also in way compatible with makefiles / scons
* invocation and customization of pipeline transformations such as running pandoc to translate markdown into HTML

## Document

The Document class parses a text into a sequence of content + magicwords. Once parsed, key methods

* get_sources
* process


## class Magicword

A Magicword has...

* a priority, for ordering its applications
* A preprocess phase
* A process phase

Subclasses can/should supply a:
    
* a process_text method



* priority: Default is (0, contentindex), contentindex the order each MW is encountered.
* collect_args: set to True so that multiple instances of the same magicword will "collect" their arguments with the first encountered instance. The *pandoc* magicword uses this feature. It makes sense for a transformation that can only be applied once, and allows for instance for options to be flexibly set.


