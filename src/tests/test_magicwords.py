import unittest
from magicwords import DocumentParser, Magicword
from magicwords.core import NowMagicword
import datetime


class ConstantMagicword (Magicword):
    def preprocess (self, stack=None):
        return "CONSTANT"

class TestDocumentMethods(unittest.TestCase):

    def setUp(self):
        self.dp = DocumentParser()
        self.dp.register_magicword("magic", ConstantMagicword)
        self.dp.register_magicword("now", NowMagicword)

    def test_document(self):
        doc = self.dp.parse("{{magic}}")
        # doc.parse()
        self.assertEqual(doc.preprocess(), "CONSTANT")

    def test_now(self):
        doc = self.dp.parse("{{now|format=%Y}}").preprocess()
        self.assertEqual(doc, datetime.datetime.now().strftime("%Y"))


if __name__ == '__main__':
    unittest.main()