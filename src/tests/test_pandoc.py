import unittest
from magicwords import DocumentParser, Magicword
from magicwords.core import PandocMagicword


sample_doc_defaults = """
{{pandoc|from=markdown}}
{{pandoc|from_blank_before_header=false}}
{{pandoc|from_lists_without_preceding_blankline}}
{{pandoc|from_hard_line_breaks-space_in_atx_header}}

{{pandoc|to=html5}}
{{pandoc|to_smart}}

{{pandoc|css=lib/wefts.css}}
{{pandoc|standalone}}
{{pandoc|section-divs}}
"""

# {{pandoc|template=templates/pandoc-weft-template.html}}


class TestPandoc(unittest.TestCase):

    def setUp(self):
        self.dp = DocumentParser()
        self.dp.register_magicword("pandoc", PandocMagicword)

    def test_document(self):
        doc = self.dp.parse(sample_doc_defaults)
        pargs = doc.magicwords[0].build_pandoc_args()
        # print (pargs)
        self.assertEqual(set(pargs), set([
            '--from=markdown-blank_before_header+lists_without_preceding_blankline+hard_line_breaks-space_in_atx_header',
            '--to=html5+smart',
            '--css=lib/wefts.css',
            '--standalone',
            '--section-divs'
        ]))

    def test_document2(self):
        doc = self.dp.parse(sample_doc_defaults + """
Hello world.

""")
        text = doc.process()
        # print ("pandoc output:\n" + text)
 
if __name__ == '__main__':
    unittest.main()
