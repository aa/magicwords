import unittest

from magicwords import DocumentParser


class TestDocumentMethods(unittest.TestCase):

    # def test_upper(self):
    #     self.assertEqual('foo'.upper(), 'FOO')

    # def test_isupper(self):
    #     self.assertTrue('FOO'.isupper())
    #     self.assertFalse('Foo'.isupper())

    # def test_split(self):
    #     s = 'hello world'
    #     self.assertEqual(s.split(), ['hello', 'world'])
    #     # check that s.split fails when the separator is not a string
    #     with self.assertRaises(TypeError):
    #         s.split(2)
    def setUp(self):
        self.dp = DocumentParser()

    def test_document(self):
        doc = self.dp.parse("")
        # doc.parse()
        self.assertTrue(len(doc.parsed_contents) == 1)

    def test_document2(self):
        doc = self.dp.parse("""This is a {{test}}.""")
        # doc.parse()
        self.assertTrue(len(doc.parsed_contents) == 3)
        self.assertEqual(doc.preprocess(), """This is a (Unknown Magicword: test).""")

    def test_document3(self):
        doc = self.dp.parse("""This is a {{test|foo}}.""")
        # doc.parse()
        self.assertTrue(len(doc.parsed_contents) == 3)
        self.assertEqual(doc.preprocess(), """This is a (Unknown Magicword: test).""")

if __name__ == '__main__':
    unittest.main()