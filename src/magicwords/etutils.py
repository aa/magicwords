from xml.etree import ElementTree as ET


def textContent (elt):
    if elt.text != None:
        ret = elt.text
    else:
        ret = u""
    return ret + u"".join([ET.tostring(x, method="text", encoding="unicode") for x in elt])

def innerHTML (elt):
    if elt.text != None:
        ret = elt.text
    else:
        ret = u""
    return ret + u"".join([ET.tostring(x, method="html", encoding="unicode") for x in elt])

def parentchilditerwithindex (elt):
    for parent in elt.iter():
        for i, child in enumerate(parent):
            yield parent, child, i

def replace_elt (t, elt, tag):
    for p, c, i in parentchilditerwithindex(t):
        if c == elt:
            # print ("replacing {0} with {1}".format(elt.tag, tag), file=sys.stderr)
            newelt = ET.SubElement(p, tag)
            p.remove(elt)
            p.remove(newelt) # otherwise it's double
            p.insert(i, newelt)
            return newelt

def add_class (elt, cls):
    classes = elt.attrib.get("class", "").split()
    if cls not in classes:
        classes.append(cls)
    elt.attrib['class'] = " ".join(classes)

def remove_class (elt, cls):
    classes = elt.attrib.get("class", "").split()
    classes.remove(cls)
    elt.attrib['class'] = " ".join(classes)
