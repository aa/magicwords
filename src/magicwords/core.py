import datetime as datetime_module
import subprocess
import sys
from .magicword import Magicword


class NowMagicword (Magicword):
    # def add_args (self, format="%c"):
    #     self.format = format

    def preprocess (self, stack=None):
        return datetime_module.datetime.now().strftime(self.args.get("format", "%c"))


def remove_prefix (s, prefix):
    if s.startswith(prefix):
        return s[len(prefix):]
    return s

class PandocTocMagicword (Magicword):
    def preprocess (self, stack=None):
        ret = "{{pandoc|toc}}"
        depth = args.get("depth")
        if depth:
            ret += '{{' + f"pandoc|toc-depth={depth}" + '}}'
        return ret

class PandocMagicword (Magicword):
    collect_args = True

    def process (self, text):
        args = self.build_pandoc_args()
        p = subprocess.run(["pandoc"] + args, input=text.encode("utf-8"), capture_output=True)
        # print (p.stderr.decode("utf-8"), file=sys.stderr)
        if p.returncode == 0:
            return p.stdout.decode("utf-8")
        else:
            return p.stderr.decode("utf-8")

    def build_pandoc_args (self):
        ## Step 1: split into from, to, and general (pandoc) args
        from_args = {}
        to_args = {}
        pandoc_args = self.args.copy()
        for key in self.args:
            if key.startswith("from_"):
                from_args[remove_prefix(key, "from_")] = self.args[key]
                del pandoc_args[key]
            if key.startswith("to_"):
                to_args[remove_prefix(key, "to_")] = self.args[key]
                del pandoc_args[key]

        ## Step 2: create the actual list of args
        def map_value_to_prefix (val):
            if val:
                return "+"
            else:
                return "-"
        
        args = []
        if "from" in pandoc_args:
            args.append(f"--from={pandoc_args['from']}"+"".join([f"{map_value_to_prefix(value)}{key}" for key, value in from_args.items()]))
            del pandoc_args['from']
        if "to" in pandoc_args:
            args.append(f"--to={pandoc_args['to']}"+"".join([f"{map_value_to_prefix(value)}{key}" for key, value in to_args.items()]))
            del pandoc_args['to']
        
        use_args = pandoc_args.copy()
        for key, value in pandoc_args.items():
            if value == False:
                del use_args[key]
        
        for key, value in use_args.items():
            if value == True:
                args.append(f"--{key}")
            else:
                args.append(f"--{key}={value}")

        return args

def register_pandoc_magicwords (dp:DocumentParser):
    dp.register_magicword("pandoc", PandocMagicword)
    dp.register_magicword("toc", PandocTocMagicword)
