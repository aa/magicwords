import re
from collections import OrderedDict

class Magicword (object):
    # pattern = re.compile("(?:__(\w+)__)|(?:{{(\w+)(?:\|(.+?))?)}}")
    pattern = re.compile("(?:__(\w+)__)|(?:{{(\w+)(\:)?(?:\|(.+?))?)}}")
    collect_args = False
                        
    @classmethod
    def get_name (cls):
        if hasattr(cls, "name"):
            return cls.name
        else:
            return cls.__name__

    def get_priority (self):
        if hasattr(self, "priority"):
            return (self.priority, self.append_index)
        else:
            return (0, self.append_index)

    @classmethod
    def parse_args (cls, match):
        # print ("parse_args", match)
        ret = OrderedDict()
        for part in match.split("|"):
            part = part.strip()
            if "=" in part:
                name, value = part.split("=", 1)
                name = name.strip()
                value = value.strip()
                if value.lower() == "false":
                    value = False
                elif value.lower() == "true":
                    value = True
                ret[name] = value
            else:
                value = part
                ret[value] = True
        return ret

    @classmethod
    def get_named_subclass(cls, name):
        name = name.lower()
        for c in cls.__subclasses__():
            if hasattr(c, "name"):
                if c.name == name:
                    return c
            else:
                if c.__name__.lower() == name:
                    return c

    # @classmethod
    # def list (cls):
    #     return Path(scripts.__path__[0]).glob("*.py")
    
    def __init__ (self, doc, **args):
        self.doc = doc
        self.append_index = -1
        self.matches = []
        self.args = args

    def add_args (self, **args):
        self.args.update(**args)
        
#     def parse (self, m):
#         """
#         called in search replace while parsing document (first pass via re.sub),
#         like sub returns the text to replace the __MAGIC__ match
#         """
#         # Let the magic word remember the (original) position of the match for eventual LATER replacement
        
#         self.matches.append((m.start(), m.end()))
#         return ""
 

class UnknownMagicword (Magicword):
    def __init__(self, doc, name, **args):
        self.name = name
        super().__init__(doc, **args)
        
    def preprocess (self, stack=None):
        return f"(Unknown Magicword: {self.name})"

# class Include (Magicword):
#     """
#     subclass this for special pad include?
#     """

#     def add_args (self, path):
#         self.path = path

#     def evaluate(self, text, stack=None):
#         with open(self.path) as fin:
#             return fin.read()

#     def get_sources (self, stack):
#         if self.path not in stack:
#             yield self.path
#             cdoc = self.doc.__class__(self.path, self.get_text_contents())
#             for src in cdoc.get_sources(stack):
#                 yield src
#         else:
#             print ("preprocess: End of include due to cyclic reference")
            
#     def preprocess (self, stack):
#         if self.path not in stack:
#             cdoc = self.doc.__class__(self.path, self.get_text_contents())
#             output += cdoc.preprocess(stack)
#         else:
#             print ("preprocess: End of include due to cyclic reference")
