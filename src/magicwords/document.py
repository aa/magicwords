from .magicword import Magicword, UnknownMagicword
import sys

"""

Make a "PreProcess Tree"

ParsedDocument -> "", MW, "", MW, "", ...
MW -> "", MW, "" ...


foo.magic.md
=> foo.html



Magicword.preprocess => ParsedDocument (ipv str)...
Recursively call preprocess on all (parsed) MagicWords...

when an include repeats... reuse existing instance ???



"""


class ParsedDocument (object):
    def __init__(self):
        self.parsed_contents = []
        self.magicwords = []
        self.magicwords_by_name = {}
    
    def preprocess (self, stack=None) -> str:
        """
        returns: text   
        The preprocess stage allows for inclusion/transclusion.
        
        The stack is a "call-stack" that stores the id (path) of each document that (might) include another
        it's used to prevent including a source recursively (including X from within (an inclusion of) X has no effect,
        
        NB: repeated (serial) inclusion of the same source is permitted by this mechanism, and cyclic references
        should be handled properly (ie inclusion should stop at the point when a source is referenced within itself).
        
        NB: stack is immutable (by design)
        """
        if stack is None:
            stack = (self,)
        else:
            stack = stack + (self,)
        
        output = ""
        for item in self.parsed_contents:
            if isinstance(item, str):
                output += item
            else:
                # Magicword, does it have a preprocess method?
                if hasattr(item, "preprocess"):
                    output += item.preprocess(stack)
        return output
                
    def process (self, verbose=False):
        text = self.preprocess()
        # apply MAGICWORDS.process filters ... order by priority
        for mw in sorted(self.magicwords, key=lambda x: x.get_priority()):
            if hasattr(mw, "process"):
                if verbose:
                    print (f"mw.Document process {mw.get_name()}", file=sys.stderr)
                text = mw.process(text)

        return text
    
    # def get_sources (self, stack=None):
    #     """
    #     yields: sources as reported by the parsed magicwords
        
    #     uses a call stack to deal with evt cyclic includes
    #     """
    #     print(f"{self}.get_sources, stack: {stack}")
    #     if stack is None:
    #         stack = (self.id,)
    #     else:
    #         stack = stack + (self.id,)
    #     for item in self.parsed_contents:
    #         if hasattr(item, "get_sources"):
    #             for src in item.get_sources(stack):
    #                 yield src

    # first implementation used something like this...
    # with the idea of centralizing metadata (not in magicword itself)
    def add_metadata_to_namespace (self, namespace, **args):
        if namespace not in self.namespaces:
            self.namespaces[namespace] = {}
        d = self.namespaces[namespace]
        for key in args:
            d[key] = args[key]
            

class DocumentParser (object):
    def __init__ (self):
        self.classes_by_name = {}

    def filter_class_name (self, name):
        return name.strip().lower()

    def register_magicword (self, name:str, klass):
        fn = self.filter_class_name(name)
        # print (f"registering magicword {fn}")
        self.classes_by_name[fn] = klass

    def get_class_by_name(self, name):
        return self.classes_by_name.get(self.filter_class_name(name))

    def parse (self, text) -> ParsedDocument:
        """
        Parse a text for magicwords...
        """
        doc = ParsedDocument()
        parts = Magicword.pattern.split(text)
        # print (f"DocumentParser.parse, parts:{len(parts)}")
        np = len(parts)
        i = 0
        while i<np:
            # print (f"parse {i}")
            doc.parsed_contents.append(parts[i])
            i += 1
            if i<np:
                mw1, mw2, colon, mw_args = parts[i:i+4]
                i += 4
                # print ("*", mw1, mw2, colon, mw_args)
                mw_name = mw1 or mw2
                mw_name = mw_name.lower()
                if mw_args:
                    mw_args = Magicword.parse_args(mw_args)
                else:
                    mw_args = {}

                mw = None
                if mw_name in doc.magicwords_by_name and doc.magicwords_by_name[mw_name][0].collect_args:
                    # if it's a repeated MW with collect_args...
                    # reuse the existing instance (add_args)
                    mw = doc.magicwords_by_name[mw_name][0]
                    mw.add_args(**mw_args)
                else:
                    # ... else make a new MW instance
                    mw_class = self.get_class_by_name(mw_name)
                    if mw_class is not None:
                        # if name matches a registered class...
                        # instantiate an instance
                        # print (f"Instantiating {mw_class.__name__}")
                        mw = mw_class(doc, **mw_args)
                        if mw_name not in doc.magicwords_by_name:
                            doc.magicwords_by_name[mw_name] = []
                        doc.magicwords_by_name[mw_name].append(mw)
                    else:
                        # ... else use UnknownMagicword
                        mw = UnknownMagicword(self, mw_name, **mw_args)
                    # In either case
                    # set the append_index (index in magicwords)
                    # and push on to magicwords
                    mw.append_index = len(doc.magicwords)
                    doc.magicwords.append(mw)
                # ADD Magicword to pc
                doc.parsed_contents.append(mw)
        return doc



