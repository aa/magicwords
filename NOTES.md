# TODO
* ability to remove / undo a magicword
* As used for etherpads, it's handy to be able to add arbitrary metadata (like the last edited date). How to design a generic metadata magicword that then works with metadata stored in the Document object? This was the earlier "namespaces" dictionary.
* Test includes!
* allow general recursion to make convenience forms like {{TOC}} => {{pandoc|toc}}


